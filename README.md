Block demo for zx spectrum.
==========================

It supports drawing blocks, and applies "ray trace" lighting, according to how dense each block type is.

For compilation instructions see [my spaceship game project](https://gitlab.com/wilsonco-moo/zx-spectrum-spaceship).

One day I'll make a platform game out of this.

![Image](development/screenshot.png)
