#include <algorithm>
#include <iostream>
#include <cstdint>
#include <array>
#include <cmath>

constexpr static unsigned int RADIUS = 8;
constexpr static unsigned int SIZE = 2 * RADIUS + 1;
constexpr static unsigned int CELL_SIZE = 16;

using image_t = std::array<uint8_t, SIZE*SIZE*CELL_SIZE*CELL_SIZE*3>;

void fillCell(image_t & image, unsigned int x, unsigned int y, uint8_t col) {
    unsigned int outerOffset = y * SIZE * CELL_SIZE * CELL_SIZE * 3 + x * CELL_SIZE * 3;
    for (unsigned int iy = 0; iy < CELL_SIZE; iy++) {
        for (unsigned int ix = 0; ix < CELL_SIZE; ix++) {
            unsigned int offset = outerOffset + (iy * SIZE * CELL_SIZE + ix) * 3;
            image[offset + 0] = col;
            image[offset + 1] = col;
            image[offset + 2] = col;
        }
    }
}

void printImg(const image_t & image) {
    std::cout << "P3\n" << (SIZE * CELL_SIZE) << ' ' << (SIZE * CELL_SIZE) << "\n255\n";
    for (unsigned int i = 0; i < SIZE*SIZE*CELL_SIZE*CELL_SIZE; i++) {
        std::cout << (unsigned int)image[i*3 + 0] << ' ' << (unsigned int)image[i*3 + 1] << ' ' << (unsigned int)image[i*3 + 2] << '\n';
    }
}

// https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
void plotLine(image_t & image, unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1, uint8_t red = 255, uint8_t green = 128, uint8_t blue = 0) {
    int dx = std::abs((int)x1 - (int)x0), dy = -std::abs((int)y1 - (int)y0),
        sx = x0 < x1 ? 1 : -1, sy = y0 < y1 ? 1 : -1,
       err = dx+dy;  /* error value e_xy */
    while (true) {
        unsigned int offset = (y0 * SIZE * CELL_SIZE + x0) * 3;
        image[offset + 0] = red; image[offset + 1] = green; image[offset + 2] = blue;
        if (x0 == x1 && y0 == y1) break;
        int e2 = 2 * err;
        if (e2 >= dy) { /* e_xy+e_x > 0 */
            err += dy; x0 += sx;
        }
        if (e2 <= dx) { /* e_xy+e_y < 0 */
            err += dx; y0 += sy;
        }
    }
}

void plotCells(image_t & image, unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1) {
    plotLine(image, x0 * CELL_SIZE + CELL_SIZE / 2, y0 * CELL_SIZE + CELL_SIZE / 2, x1 * CELL_SIZE + CELL_SIZE / 2, y1 * CELL_SIZE + CELL_SIZE / 2);
}

int main(void) {
    image_t img({0});
    
    for (unsigned int iy = 0; iy < SIZE; iy++) {
        for (unsigned int ix = 0; ix < SIZE; ix++) {
            int dx = ((int)ix - (int)RADIUS);
            int dy = ((int)iy - (int)RADIUS);
            unsigned int dist = std::floor(std::sqrt(dx*dx + dy*dy));
            if (dist > RADIUS) continue;
            
            fillCell(img, ix, iy, 255 - ((dist * 255) / (RADIUS + 1)));
        }
    }
    
    for (unsigned int iy = RADIUS; iy < SIZE; iy++) {
        for (unsigned int ix = RADIUS; ix < SIZE; ix++) {
            int dx = ((int)ix - (int)RADIUS);
            int dy = ((int)iy - (int)RADIUS);
            unsigned int dist = std::floor(std::sqrt(dx*dx + dy*dy));
            if (dist > RADIUS) continue;
            
            if (dx * 2 < dy) {
                plotCells(img, ix, iy - 1, ix, iy);
            } else if (dy * 2 < dx) {
                plotCells(img, ix - 1, iy, ix, iy);
            } else if (dx != 0 && dy != 0) {
                plotCells(img, ix - 1, iy - 1, ix, iy);
            }
        }
    }
    printImg(img);
    return 0;
}
