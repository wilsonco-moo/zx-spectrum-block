import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

class ImageConv {
    private static final int WIDTH = 32, HEIGHT = 22;
    
    
    public static void main(String[] args) throws Exception {
        
        System.out.print("defb ");
        
        BufferedImage image = ImageIO.read(new File("img.png"));
        
        for (int iy = 0; iy < HEIGHT; iy++) {
            for (int ix = 0; ix < WIDTH; ix++) {
                
                int colour = image.getRGB(ix, iy);
                
                int brightness = (255 - ((colour & 0x0000ff00) >> 8)) / 64;
                int blockId = ((colour & 0x00ff0000) >> 16) / 32;
                
                int output = ((blockId & 0b111) << 5) | ((brightness & 0b11) << 3) | ((blockId & 0b111000) >> 3);
                
                System.out.print("" + output + ((ix == WIDTH - 1 && iy == HEIGHT - 1) ? "" : ", "));
            }
        }
        
        System.out.println();
    }
}

