; Block utilities.
; Format for packed block bytes:
;   210 ll 543
; Where 5,4,3,2,1,0 are the 6 bits of the block ID
; And ll are the two bits of the light level.
;
; 39 bytes currently
; 
; block_*


; Masks out the light level from a packed block byte.
block_LIGHT_LEVEL_MASK: equ 00011000b

; Masks out the block id from a packed block byte.
block_BLOCK_ID_MASK: equ 11100111b


; Converts block id and light level into a packed block byte.
; 
; Parameters:
;       d: Light level (2 bits!)
;       e: Block ID (6 bits!)
; Modifies:
;       a
; Returns:
;       a: Packed output.
;
block_pack:
    ; Rotate light level to be most significant two bits, mask.
    ld a, d
    rrca
    rrca
    and 11000000b
    
    ; Or in the block id, rotate three times so light level is in middle.
    or e
    rrca
    rrca
    rrca
ret

; Unpacks block ID from a packed block byte.
; 
; Parameters:
;       e: Packed block byte
; Modifies:
;       a
; Returns:
;       a: Block ID.
;
block_unpackId:
    ; Rotate left so block id is least significant bits.
    ld a, e
    rlca
    rlca
    rlca
    
    ; Mask.
    and 00111111b
ret

; Unpacks light level from a packed block byte.
; 
; Parameters:
;       e: Packed block byte
; Modifies:
;       a
; Returns:
;       a: Block ID.
;
block_unpackLightLevel:
    ; Rotate right so light level is least significant bits.
    ld a, e
    rrca
    rrca
    rrca
    
    ; Mask.
    and 00000011b
ret

; Utility macro which writes the offset of the block specified by packed byte e,
; to hl.
; (8 clock cycles).
; 
; Parameters:
;       e: Packed block byte
; Modifies:
;       a, hl
; Returns:
;       hl: Address of block.
;
block_getAddressOffsetMacro: macro
    ; Load bits 3,4,5 into h
    ld a, e             ; 1
    and 00000111b       ; 2
    ld h, a             ; 1
    
    ; Load bits 0,1,2 into l
    ld a, e             ; 1
    and 11100000b       ; 2
    ld l, a             ; 1
endm

; Gets address of block properties, for the specified packed block byte.
; Note that this is NOT the same as the address of the actual block, see
; blockData.
; This should be used as the general purpose way to access block properties.
; 
; Parameters:
;       a: Packed block byte
; Modifies:
;       a, hl
; Returns:
;       hl: Address of block properties.
;
block_getProperties:
    push de
    
    ; Put block byte into e temporarily.
    ld e, a
    
    ; Load bits 3,4,5 into h
    and 00000111b       ; 2
    ld h, a             ; 1
    
    ; Load bits 0,1,2 into l
    ld a, e             ; 1
    and 11100000b       ; 2
    ld l, a             ; 1
    
    ; Add offset of block properties to hl.
    ld de, blockData_properties
    add hl, de
    
    pop de
ret
