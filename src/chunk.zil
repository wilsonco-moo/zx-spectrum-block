; Utilities and constants for chunks.
; 193 bytes currently.
;
; chunk_*
;
; 76543210 vs 21076543
;
;   -> block array
;
;   21076543 21076543
;   ...76543_210xxxxx
;
;   -> sinclair screen (n is constant 010)
;   nnn76210_543xxxxx
;   -> screen ignoring lower 3 bits
;
;   21076543 21076543
;   nnn43..._210xxxxx



; Chunk size (two less on the bottom for UI)
CHUNK_WIDTH:  equ SCREEN_COLS
CHUNK_HEIGHT: equ SCREEN_ROWS - 2

; Chunk width is a power of two, so can be masked.
CHUNK_WIDTH_MASK: equ SCREEN_COLS - 1
CHUNK_WIDTH_INV_MASK: equ 255 - CHUNK_WIDTH_MASK


; Gets the offset within a chunk of the specified x and y position.
; 9 clock cycles, this is a macro so should only be used in code where
; performance is very important. Otherwise use chunk_getPackedBlock.
;
; Parameters:
;       d: X position within chunk
;       e: Y position within chunk (rotated three bits to the right!!).
; Modifies:
;       a, bc
; Returns:
;       bc: Offset within chunk.
;
chunk_getOffsetMacro: macro
    ; Mask out 5 top bits, put in b.
    ld a, e         ; 1
    and 00011111b   ; 2
    ld b, a         ; 1
    
    ; Mask out 3 bottom bits, or in x position, put in c.
    ld a, e         ; 1
    and 11100000b   ; 2
    or d            ; 1
    ld c, a         ; 1
endm


; Accesses the packed block at the specified position within the chunk.
; This should be used as the general purpose way to access blocks within chunks.
; Note that it is very convenient to pair a call to this with a call to
; block_getProperties, to get information about a position within a chunk.
;
; Parameters:
;       d: X position within chunk
;       e: Y position within chunk.
;      hl: Base address of chunk.
; Modifies:
;       a
; Returns:
;       a: Packed block at specified location.
;
chunk_getPackedBlock:
    push hl
    call chunk_getBlockAddress
    ld a, (hl)
    pop hl
ret

; Accesses the address of the block at the specified position within the chunk.
; This should be used a the general purpose way to access/write blocks within
; chunks. If only access is required, use chunk_getPackedBlock instead.
;
; Parameters:
;       d: X position within chunk
;       e: Y position within chunk.
;      hl: Base address of chunk.
; Modifies:
;       a, hl
; Returns:
;      hl: Address of block.
;
chunk_getBlockAddress:
    push bc
    
    ; Get offset within chunk into bc.
    ; Rotate y three bits to the right, keep temporarily in c.
    ld a, e
    rrca
    rrca
    rrca
    ld c, a
    
    ; Mask out 5 top bits, put in b.
    and 00011111b
    ld b, a
    
    ; Mask out 3 bottom bits, or in x position, put in c.
    ld a, c
    and 11100000b
    or d
    ld c, a
    
    ; Add offset to chunk address.
    add hl, bc
    
    pop bc
ret




; Draws a single block to the screen.
; Note: If drawing more than one block, use chunk_drawBlockRow.
;
; Parameters:
;      hl: Base address of chunk.
;       d: Block X position within chunk
;       e: Rotated block Y position within chunk
; Modifies:
;       a, af', bc', de', hl'
; Returns:
;       None
;
chunk_drawBlock:
    push bc
    ld c, 1
    call chunk_drawBlockRow
    pop bc
ret


; Draws a horizontal row of blocks. This is faster than drawing a single block,
; as it can re-use things like screen address and chunk offsets between blocks.
;
; Benchmark results show that the speed advantage of re-using data between
; blocks, outweighs the overhead of drawing colours, e.g:
; 128 screen refreshes of basic per-block function (without colour support): 16 seconds.
; 128 screen refreshes of chunk_drawWholeChunk (with colour support): 14 seconds
;
; Parameters:
;      hl: Base address of chunk.
;       c: Number of blocks in row, must be at least 1.
;       d: Block X position within chunk
;       e: Y position within chunk
; Modifies:
;       a, af', bc', de', hl'
; Returns:
;       None
;
chunk_drawBlockRow:
    push bc
    push de
    push hl
    
    ; --- Setup ---
    
    ; Put number of blocks in the row into a'.
    ld a, c
    ex af, af'
    
    ; Put upper byte of screen address into b'. This is from 4,3 of y
    ; position, or'ed with the top byte of the base address of the display buffer.
    ld a, e
    and 00011000b
    or SCREEN_MEMSTART_L
    exx
    ld b, a
    exx
    
    ; Rotate y position right three bits, as it simplifies all subsequent calculations.
    ld a, e
    rrca
    rrca
    rrca
    ld e, a
    
    ; Get lower byte of block's screen address, into c'. - this is rotated y bits 7,6,5
    ; (original bits 2,1,0), or'ed with the x position.
    and 11100000b
    or d
    exx
    ld c, a
    exx
    
    ; Put in bc the offset within the chunk. This is calculated from x and rotated y (currently in de).
    chunk_getOffsetMacro
    
    ; Put chunk base address in de, (currently in hl).
    ex de, hl
    
    ; Swap accumulators so loop doesn't overwrite block count.
    ex af, af'
    
    ; bc = chunk offset,  de = chunk base address,  a' = loop counter, 
    ; bc' = screen address,  d' = screen address top byte backup,  e' = block image address bottom byte backup
    
    ; --- Loop ---
    
    ; Iterate until we run out of blocks, using a' as loop counter.
    chunk_drawBlockRow_blockLoop:
        ex af, af'
        
        ; Get address of block within chunk, load packed block.
        ld h, b
        ld l, c
        add hl, de
        ld a, (hl)
        
        ; Extract light level, compare.
        and 00011000b
        cp 00011000b
        jr nz, chunk_drawBlockRow_lit
            
            ; For light level 11 (dark)
            
            ; Load zero (black).
            xor a
            
            exx
            
            ; Keep a backup of top byte of screen address
            ld d, b
            
            ; Draw 8 bytes of the same thing (black).
            ld (bc), a
            inc b
            ld (bc), a
            inc b
            ld (bc), a
            inc b
            ld (bc), a
            inc b
            ld (bc), a
            inc b
            ld (bc), a
            inc b
            ld (bc), a
            inc b
            ld (bc), a
            
            ; Restore top byte of screen address, increment bottom byte of screen address for next time.
            ld b, d
            inc c
            
            exx
        
        jr chunk_drawBlockRow_endif
        chunk_drawBlockRow_lit:
            
            ; For light levels 00, 01, 10 (not dark).
            
            ; Re-load packed block, since masking and comparing destroyed its value.
            ld a, (hl)
            
            exx
            
            ; Backup packed block in e.
            ld e, a
            
            ; Load block id bits 5,4,3 into h
            and 00000111b       ; 2
            ld h, a             ; 1
            
            ; Load block id bits 2,1,0 then light level into most significant 5 bits of l.
            ld a, e             ; 1
            and 11111000b       ; 2
            ld l, a             ; 1
            
            ; Add base address of block data to hl, now we have address of block image.
            ld de, blockData
            add hl, de
            
            ; Keep a backup of top byte of screen address (e <- l), and bottom byte of block image address (d <- b), for use later.
            ld e, l
            ld d, b
            
            ; Copy across the 8 bytes of block data.
            ld a, (hl)
            ld (bc), a
            inc l
            inc b
            
            ld a, (hl)
            ld (bc), a
            inc l
            inc b
            
            ld a, (hl)
            ld (bc), a
            inc l
            inc b
            
            ld a, (hl)
            ld (bc), a
            inc l
            inc b
            
            ld a, (hl)
            ld (bc), a
            inc l
            inc b
            
            ld a, (hl)
            ld (bc), a
            inc l
            inc b
            
            ld a, (hl)
            ld (bc), a
            inc l
            inc b

            ld a, (hl)
            ld (bc), a
            
            ; Restore top byte of screen address, increment bottom byte of screen address for next time.
            ld b, d
            inc c
            
            ; Shift and mask out the light level bits to the least significant bits, so they're
            ; a 0-2 offset. Or in the original low address byte, then set original light level
            ; bits both to one (forms an offset of 24). This gets the address of the block type's
            ; colour at current light level.
            ld a, e
            rrca
            rrca
            rrca
            and 00000011b
            or e
            or 00011000b
            
            ; Load block colour from address we just worked out. (We still have h from before
            ; we did drawing, which wasn't incremented).
            ld l, a
            ld a, (hl)
            
            exx
            
        chunk_drawBlockRow_endif:
        
        ; Store block colour into appropriate place in colour buffer (colour buffer plus chunk offset).
        ; Note that the block colour is left in a, regardless of whether block is black or not, so this code can be shared.
        ld hl, SCREEN_COLSTART
        add hl, bc
        ld (hl), a
        
        ; Increment bottom byte of chunk offset for next time.
        inc c        
        
        ; Loop counter.
        ex af, af'
        dec a
        jr nz, chunk_drawBlockRow_blockLoop
        
    pop hl
    pop de
    pop bc
ret

; Draws all blocks in a chunk.
; Note: If only some of the blocks actually need re-drawing, use
; chunk_drawBlockRow or chunk_drawBlock instead.
;
; Parameters:
;      hl: Base address of chunk.
; Modifies:
;       a, af', bc', de', hl'
; Returns:
;       None
;
chunk_drawWholeChunk:
    push de
    push bc
    
    ld de, 0 ; x,y
    ld bc, (CHUNK_HEIGHT << 8) | CHUNK_WIDTH ; loop counter (row count), row width.
    
    chunk_drawWholeChunk_loop:
        
        ; Draw each row.
        call chunk_drawBlockRow
    
        inc e
        dec b
        jr nz, chunk_drawWholeChunk_loop
    
    pop bc
    pop de
ret


