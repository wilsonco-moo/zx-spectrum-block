; REGISTER USE POLICY
; 
; There are two kinds of function:
;
; Well-behaved function
; *********************
;  - This uses the system stack to preserve the values of any registers
;    that is uses, except for a and optionally hl
;  - The accumulator is the only register that a well-behaved method
;    does not need to preserve. It can preserve it, but the documentation
;    should specify this.
;  - Well-behaved methods should take their parameters from registers,
;    the system stack, or a memory location. This should be specified in
;    it's associated documentation.
;  - The return value can also be a register, memory location or the
;    system stack. This should be specified in the documentation.
;
; Simple function
; ***************
;  - This is allowed to leave registers with a different value than they
;    started with.
;  - Any registers modified in the method body should be specified in the
;    associated documentation.
;  - The function should take it's parameters from registers, and use
;    registers for the return value. The system stack should not be used,
;    as simple methods are intended to execute quickly.
;
;
; Further register use info
; *************************
;
; Any simple function can be automatically expected to overwrite register a,
; as this is the accumulator. Almost all simple functions can be automatially
; expected to overwrite h,l   (hl) - the 16-bit address register. As such,
; a, h and l should be used for temporary values in functions.
;  -- This should be specified in the documentation. ^
;
; Return values of functions should be a,h,l in preference to other registers,
; as these are special registers, and generally then load operations would not
; be needed to move values around in the code calling the function.
; The register(s) used should be specified in documentation.
;
; Use of other registers
; **********************
;
; The other general purpose registers are in the order:
; a, b, c, d, e
; These should be used as function parameters, starting from the end of the list.
; For example, if a function takes one parameter, it should use register e.
; If a function takes two parameters, it should use registers d and e.
;    .. etc.
;
; If the function needs any other temporary values than a, h, l, it should 
; use last registers before it's parameters. For example, if a function needs one
; temporary value, and it takes one parameter, it should use e as it's parameter,
; and use d as the temporary value. All of this should be specified in it's documentation.
;
;


; ------------------------------ screen ----------------------------

SCREEN_WIDTH:       equ    256
SCREEN_HEIGHT:      equ    192
SCREEN_AREA:        equ    SCREEN_WIDTH * SCREEN_HEIGHT

SCREEN_COLS:        equ    SCREEN_WIDTH/8
SCREEN_ROWS:        equ    SCREEN_HEIGHT/8
SCREEN_CELLS:       equ    SCREEN_COLS * SCREEN_ROWS

; For main display buffer (size, start address, end address).
SCREEN_BYTES:       equ    SCREEN_AREA / 8
SCREEN_BYTES_R:     equ    SCREEN_BYTES & 0x00ff
SCREEN_BYTES_L:     equ    (SCREEN_BYTES & 0xff00) >> 8

SCREEN_MEMSTART:    equ    16384
SCREEN_MEMSTART_R:  equ    SCREEN_MEMSTART & 0x00ff
SCREEN_MEMSTART_L:  equ    (SCREEN_MEMSTART & 0xff00) >> 8

SCREEN_MEMEND:      equ    SCREEN_MEMSTART + SCREEN_BYTES
SCREEN_MEMEND_R:    equ    SCREEN_MEMEND & 0x00ff
SCREEN_MEMEND_L:    equ    (SCREEN_MEMEND & 0xff00) >> 8

; For colour buffer (size, start address, end address).
SCREEN_COL_BYTES:   equ    SCREEN_CELLS
SCREEN_COL_BYTES_R: equ    SCREEN_COL_BYTES & 0x00ff
SCREEN_COL_BYTES_L: equ    (SCREEN_COL_BYTES & 0xff00) >> 8

SCREEN_COLSTART:    equ    SCREEN_MEMEND
SCREEN_COLSTART_R:  equ    SCREEN_COLSTART & 0x00ff
SCREEN_COLSTART_L:  equ    (SCREEN_COLSTART & 0xff00) >> 8

SCREEN_COLEND:      equ    SCREEN_COLSTART + SCREEN_COL_BYTES
SCREEN_COLEND_R:    equ    SCREEN_COLEND & 0x00ff
SCREEN_COLEND_L:    equ    (SCREEN_COLEND & 0xff00) >> 8
