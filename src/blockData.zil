; Data for block types, and block IDs.
;
; blockData*, blockId_*


; Align blockData to 32 bytes.
blockData_before:
defs (blockData_before & 0x1f) == 0 ? 0 : (blockData_before & 0xffe0) + 32 - blockData_before

; Block data defines everything about each block type.
blockData:

; Base address of block properties (the part of data containing useful stuff - i.e: not the texture).
blockData_properties: equ blockData + 27

blockId_stone: equ 0
    defb 00000000b, 00010011b, 11100000b, 00000000b, 00011010b, 00000000b, 11000001b, 00011100b ; Bright
    defb 00000000b, 00010011b, 11110000b, 00000000b, 00011110b, 00000000b, 11000001b, 00011100b ; Medium
    defb 01100000b, 00010011b, 11111000b, 00000001b, 00111110b, 00000000b, 11000001b, 00111100b ; Dim
    defb 01111000b              ; Colour (bright)
    defb 00111000b              ; Colour (medium)
    defb 00000111b              ; Colour (dim)
    defb 0                      ; Luminosity (how bright the block is)
    defb 63                    ; Density (0-255, how opaque the blocks is)
    defs 3                      ; filler
    
blockId_dirt: equ 1
    defb 00010001b, 00000100b, 00000000b, 00100000b, 00001000b, 10000000b, 00010001b, 00000000b ; Bright
    defb 00010001b, 00000000b, 00000000b, 00100000b, 00000000b, 10000000b, 00010001b, 00000000b ; Medium
    defb 00010001b, 00000000b, 00000000b, 00100000b, 00000000b, 10000000b, 00010001b, 00000000b ; Dim
    defb 01010100b              ; Colour (bright)
    defb 00010100b              ; Colour (medium)
    defb 00000100b              ; Colour (dim)
    defb 0                      ; Luminosity (how bright the block is)
    defb 40                     ; Density (0-255, how opaque the blocks is)
    defs 3                      ; filler

blockId_grass: equ 2
    defb 11111111b, 11110111b, 10111001b, 10101010b, 01000101b, 10000000b, 00010001b, 00000000b ; Bright
    defb 11111111b, 11110111b, 10111000b, 00100010b, 00000000b, 10000000b, 00010001b, 00000000b ; Medium
    defb 11111111b, 01010011b, 10101000b, 00100010b, 00000000b, 10000000b, 00010001b, 00000000b ; Dim
    defb 01010100b              ; Colour (bright)
    defb 00010100b              ; Colour (medium)
    defb 00000100b              ; Colour (dim)
    defb 0                      ; Luminosity (how bright the block is)
    defb 40                     ; Density (0-255, how opaque the blocks is)
    defs 3                      ; filler

blockId_sky: equ 3
    defb 00000000b, 00000000b, 00000000b, 00000000b, 00000000b, 00000000b, 00000000b, 00000000b ; Bright
    defb 00000000b, 00000000b, 00000000b, 00000000b, 00000000b, 00000000b, 00000000b, 00000000b ; Medium
    defb 00000000b, 00000000b, 00000000b, 00000000b, 00000000b, 00000000b, 00000000b, 00000000b ; Dim
    defb 01101000b              ; Colour (bright)
    defb 00101000b              ; Colour (medium)
    defb 00101000b              ; Colour (dim)
    defb 255                    ; Luminosity (how bright the block is)
    defb 20                     ; Density (0-255, how opaque the blocks is)
    defs 3                      ; filler

blockId_undergroundSky: equ 4
    defb 00000000b, 00100000b, 00000010b, 00000000b, 00000000b, 01000000b, 00000100b, 00000000b ; Bright
    defb 01000100b, 00010001b, 10001000b, 00100010b, 01000100b, 00010001b, 10001000b, 00100010b ; Medium
    defb 01000100b, 00010001b, 10001000b, 00100010b, 01000100b, 00010001b, 10001000b, 00100010b ; Dim
    defb 01101001b              ; Colour (bright)
    defb 00101001b              ; Colour (medium)
    defb 00000101b              ; Colour (dim)
    defb 0                      ; Luminosity (how bright the block is)
    defb 20                     ; Density (0-255, how opaque the blocks is)
    defs 3                      ; filler
