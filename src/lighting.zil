; Utilities for lighting.
; 275 bytes currently.
;
; blockData*, blockId_*


; Has following means that the next block along (horizontally) immediately follows
; in the lighting data.
lighting_HAS_FOLLOWING_BIT: equ 6
lighting_HAS_FOLLOWING: equ 1 << lighting_HAS_FOLLOWING_BIT

; Has branch means that the block diagonally below is at the offset specified by
; lower bits, within lighting data.
lighting_HAS_BRANCH_BIT: equ 7
lighting_HAS_BRANCH: equ 1 << lighting_HAS_BRANCH_BIT

; Mask for offset in lower bits.
lighting_OFFSET_MASK: equ 00111111b

; format:
; mode | branch offset
lighting_data:
    defb lighting_HAS_FOLLOWING | lighting_HAS_BRANCH | (33 - 0) ; pos: 0
    defb lighting_HAS_FOLLOWING | lighting_HAS_BRANCH | (9 - 1) ; pos: 1
    defb lighting_HAS_FOLLOWING ; pos: 2
    defb lighting_HAS_FOLLOWING ; pos: 3
    defb lighting_HAS_FOLLOWING ; pos: 4
    defb lighting_HAS_FOLLOWING ; pos: 5
    defb lighting_HAS_FOLLOWING ; pos: 6
    defb lighting_HAS_FOLLOWING ; pos: 7
    defb 0 ; pos: 8
    defb lighting_HAS_FOLLOWING | lighting_HAS_BRANCH | (29 - 9) ; pos: 9
    defb lighting_HAS_FOLLOWING | lighting_HAS_BRANCH | (16 - 10) ; pos: 10
    defb lighting_HAS_FOLLOWING ; pos: 11
    defb lighting_HAS_FOLLOWING ; pos: 12
    defb lighting_HAS_FOLLOWING ; pos: 13
    defb lighting_HAS_FOLLOWING ; pos: 14
    defb 0 ; pos: 15
    defb lighting_HAS_FOLLOWING | lighting_HAS_BRANCH | (26 - 16) ; pos: 16
    defb lighting_HAS_FOLLOWING | lighting_HAS_BRANCH | (21 - 17) ; pos: 17
    defb lighting_HAS_FOLLOWING ; pos: 18
    defb lighting_HAS_FOLLOWING ; pos: 19
    defb 0 ; pos: 20
    defb lighting_HAS_FOLLOWING | lighting_HAS_BRANCH | (25 - 21) ; pos: 21
    defb lighting_HAS_FOLLOWING | lighting_HAS_BRANCH | (24 - 22) ; pos: 22
    defb 0 ; pos: 23
    defb 0 ; pos: 24
    defb 0 ; pos: 25
    defb lighting_HAS_BRANCH | (27 - 26) ; pos: 26
    defb lighting_HAS_BRANCH | (28 - 27) ; pos: 27
    defb 0 ; pos: 28
    defb lighting_HAS_BRANCH | (30 - 29) ; pos: 29
    defb lighting_HAS_BRANCH | (31 - 30) ; pos: 30
    defb lighting_HAS_BRANCH | (32 - 31) ; pos: 31
    defb 0 ; pos: 32
    defb lighting_HAS_BRANCH | (34 - 33) ; pos: 33
    defb lighting_HAS_BRANCH | (35 - 34) ; pos: 34
    defb lighting_HAS_BRANCH | (36 - 35) ; pos: 35
    defb lighting_HAS_BRANCH | (37 - 36) ; pos: 36
    defb lighting_HAS_BRANCH | (38 - 37) ; pos: 37
    defb 0 ; pos: 38
lighting_data_end:

; Make sure lighting data doesn't cross 256 byte boundary (otherwise we would need
; to do add with carry in all the arithmetic). Don't define space to align as that
; would be wasteful. Re-ordering includes can usually just be done to satisfy this.
if (lighting_data & 0xff00) != ((lighting_data_end - 1) & 0xff00)
    ERROR: Lighting data must not cross 256 byte boundary, try re-ordering includes.
endif



lighting_test:
    push de
    push bc
    
    ld c, CHUNK_WIDTH
    
    ld e, 0
    lighting_test_outerLoop:
        
        ld d, 0
        lighting_test_innerLoop:
            
            call lighting_lightBlock
            call chunk_drawBlock
            
        inc d
        ld a, d
        cp CHUNK_WIDTH
        jr c, lighting_test_innerLoop
    
    inc e
    ld a, e
    cp CHUNK_HEIGHT
    jr c, lighting_test_outerLoop
    
    
    pop bc
    pop de
ret



; Runs lighting calculation on a block.
;
; Parameters:
;      hl: Chunk base address.
;       d: Block x position
;       e: Block y position
; Modifies:
;       a, a'
; Returns:
;       None.
lighting_lightBlock:
    push hl
    push bc
    
    ; c' is maximum accumulated light.
    exx
    ld c, 0
    exx
    
    ; Iterate 8 times.
    ld bc, lighting_data
    ld a, 7
    lighting_testLoop:
        
        ; Set iteration mode from a.
        call lighting_setMode
        
        ; Set contribution to maximum each time.
        exx
        ld e, 255
        exx
        
        ; Run recursive call (preserve loop counter).
        push af
        call lighting_internal
        pop af
        
    dec a
    jp p, lighting_testLoop
    
    ; Get packed block, mask block id.
    call chunk_getBlockAddress
    ld a, (hl)
    and block_BLOCK_ID_MASK
    ld b, a
    
    ; Get maximum accumulated light, negate it, shift right, mask out light
    ; level bits, or in the block id, store back.
    exx
    ld a, 255
    sub c
    exx
    rrca
    rrca
    rrca
    and block_LIGHT_LEVEL_MASK
    or b
    ld (hl), a
    
    pop bc
    pop hl
ret


; Recursive lighting call.
;
; Parameters:
;      hl: Chunk base address.
;       d: Block x position.
;       e: Block y position.
;      bc: Lighting data address.
;      e': Current contribution of this block.
;      c': Current maximum accumulated light.
; Modifies:
;       a, a', c', d', e', hl'
; Returns:
;      None.
;
lighting_internal:

    ; Return and do nothing if x or y position is out of range or wrapped around.
    ; Comparing x is easy - chunk width is power of two so we can inverse mask.
    ; Comparing y is harder - must force number to be positive then check out
    ; of range (so we detect out of range AND wrap-around).
    ld a, d
    and CHUNK_WIDTH_INV_MASK
    ret nz
    ld a, e
    and 01111111b
    cp CHUNK_HEIGHT
    ret p
    
    ; ----------------------- Lighting calculation -----------------------------
    
    ; Get packed block at position de within chunk, into a.
    call chunk_getPackedBlock
    
    exx
    
    ; Get address of block properties into hl.
    call block_getProperties
    
    ; Get luminosity into d, multiply it down by contribution (e), result ends up in a.
    ld d, (hl)
    call util_fractionalMultiply
    
    ; If result is greater than maximum light, set accumulated light.
    cp c
    jr c, lighting_internal_tooDim
        ld c, a
    lighting_internal_tooDim:
    
    ; Increment hl so it now points to block density (used later).
    inc hl
    
    
    exx
    
    
    
    
    ;exx
    ;push bc
    ;push de
    ;push hl
    ;exx
    ;util_wait 10
    ;call chunk_drawBlock
    ;exx
    ;pop hl
    ;pop de
    ;pop bc
    ;exx
    
    
    
    
    ; -------------------------- Recursive calling -----------------------------
    
    ; If this position has a following:
    ld a, (bc)
    bit lighting_HAS_FOLLOWING_BIT, a
    jr z, lighting_internal_noFollowing
        
        exx
        
        ; Work out contribution minus one (e) minus density (block data, d).
        ; Use minus one so we can do less than or equal calculations easily.
        ld a, e
        dec a
        ld d, (hl)
        sub d
        
        ; Bail out if density pushed contribution to zero or below, or if this is less than or
        ; equal to accumulated maximum light (c). (In this case, nothing we recurse to down this
        ; path would find anything brighter than what we already have).
        jr c, lighting_internal_followingTooDim
        cp c
        jr c, lighting_internal_followingTooDim
        
        ; Preserve de and hl since the recursive call will overwrite it (and it might be needed if we have a branch).
        push hl
        push de
        
        ; Set contribution from what we just worked out, (re-increment it).
        ld e, a
        inc e
        
        exx
        
        ; Move lighting address and block address one block forward, recurse.
        ; After recursing move them back.
        inc bc
        lighting_internal_primaryForward0: inc d
        call lighting_internal
        lighting_internal_primaryBackward0: dec d
        dec bc
    
        exx
        ; Matching pops from above.
        pop de
        pop hl
        lighting_internal_followingTooDim:
        exx
    
    lighting_internal_noFollowing:
    
    ; Return if this position has no branch.
    ld a, (bc)
    bit lighting_HAS_BRANCH_BIT, a
    ret z
    
    
    exx
    
    ; Load density, multiply by 1.5 ~= sqrt(2) since we're going diagnonally.
    ld d, (hl)
    ld a, d
    srl d
    add d
    ld d, a
    
    ; Work out contribution minus one (e) minus density (d).
    ; Use minus one so we can do less than or equal calculations easily.
    ld a, e
    dec a
    sub d
    
    ; Bail out if density pushed contribution to zero or below, or if this is less than or
    ; equal to accumulated maximum light (c).
    jr c, lighting_internal_branchTooDim
    cp c
    jr c, lighting_internal_branchTooDim
    
    ; Note: We can let the recursive call overwrite hl and de, since we won't need it again.
    
    ; Set contribution from what we just worked out, (re-increment it).
    ld e, a
    inc e
    
    exx
    
    
    ; Re-load position, since above code uses accumulator, push and add offset to lighting address.
    ld a, (bc)
    push bc
    and lighting_OFFSET_MASK
    add c
    ld c, a
    
    ; Move block address, recurse, recover block address.
    ; Since this is just increment/decrement, this is faster than push/pop.
    lighting_internal_primaryForward1: inc d
    lighting_internal_secondaryForward1: inc e
    call lighting_internal
    lighting_internal_primaryBackward1: dec d
    lighting_internal_secondaryBackward1: dec e
    
    ; Recover lighting address.
    pop bc
ret
lighting_internal_branchTooDim:
    ; Must make sure we run exx before returning, if we bailed out within an exx pair.
    exx
ret



; Literal values representing "inc d", "inc e", "dec d", "dec e" instructions,
; (see zilog z80 user manual).
lighting_INC_D: equ 00010100b
lighting_INC_E: equ 00011100b
lighting_DEC_D: equ 00010101b
lighting_DEC_E: equ 00011101b

; Sets lighting mode to one of 8 modes. This modifies the code within
; lighting_internal, replacing increment/decrement x/y instructions so swap
; the direction or order. This allows iterating through octants while using
; the same data.
;
; Parameters:
;       a: Lighting mode
; Modifies:
;       f
; Returns:
;       None.
;
lighting_setMode:
    push bc
    push de
    push hl
    
    ; Load bc with primary/secondary forward (increment), load de with primary/secondary backward (decrement).
    ld bc, (lighting_INC_D << 8) | lighting_INC_E
    ld de, (lighting_DEC_D << 8) | lighting_DEC_E
    
    ; If bit 0 is set, swap over primary increment/decrement.
    bit 0, a
    jr z, lighting_setMode_noSwapPrimary
        ld h, b
        ld b, d
        ld d, h
    lighting_setMode_noSwapPrimary:
    
    ; If bit 1 is set, swap over secondary increment/decrement.
    bit 1, a
    jr z, lighting_setMode_noSwapSecondary
        ld h, c
        ld c, e
        ld e, h
    lighting_setMode_noSwapSecondary:
    
    ; If bit 2 is set, swap over primary and secondary.
    bit 2, a
    jr z, lighting_setMode_noSwapBoth
        ld h, c
        ld c, b
        ld b, h
        ld h, e
        ld e, d
        ld d, h
    lighting_setMode_noSwapBoth:
    
    ; Store primary forward.
    ld hl, lighting_internal_primaryForward0
    ld (hl), b
    ld hl, lighting_internal_primaryForward1
    ld (hl), b
    
    ; Store primary backward.
    ld hl, lighting_internal_primaryBackward0
    ld (hl), d
    ld hl, lighting_internal_primaryBackward1
    ld (hl), d
    
    ; Store secondary forward.
    ld hl, lighting_internal_secondaryForward1
    ld (hl), c
    
    ; Store secondary backward.
    ld hl, lighting_internal_secondaryBackward1
    ld (hl), e
    
    pop hl
    pop de
    pop bc
ret
