; General utility.
; 
; util_*
;
; Currently 39 bytes.


; Waits a number of "ticks", which roughly correspond to 100ths of seconds.
; Only use this for debugging.
; This is a macro so space is not wasted by this when not being used.
; Purposefully slow and inefficient to waste time.
;
; Parameters:
;      ticks: Literal count.
; Modifies:
;      None.
; Returns:
;      None.
util_wait: macro ticks
    push af
    push bc
    push de
    
    ld de, ticks
    
        ; Size = 11
        ld bc, 925
            dec bc
        xor a
        add b
        jr nz, $-3
        xor a
        add c
        jr nz, $-7
        
    dec de
    xor a
    add d
    jr nz, $-3-11
    xor a
    add e
    jr nz, $-8-11

    pop de
    pop bc
    pop af
endm

; Interprets both d and e as fractional values between 0 and 1, and multiplies
; them. For example, 255 * 255 = 255, 127 * 255 = 127, 127 * 127 = 63.
; This essentially works out the following:
; a = ((d + 1) * (e + 1)) / 256 - 1.
;
; Note: This function earlies out if d or e are 0 or 255.
;
; See: http://www.massmind.org/techref/zilog/z80/part4.htm
;      http://web.archive.org/web/20210321081755/http://www.massmind.org/techref/zilog/z80/part4.htm
;
; Parameters:
;      d: First value to multiply.
;      e: Second value to multiply.
; Modifies:
;      a
; Returns:
;      a: Multiplied result: ((d + 1) * (e + 1)) / 256 - 1
util_fractionalMultiply:
    
    ; Early out if either operand is zero.
    xor a
    cp d
    ret z
    cp e
    ret z
    
    ; Early out and return opposite if either operand is 255.
    dec a
    cp d
    jr z, util_fractionalMultiply_returnE
    cp e
    jr z, util_fractionalMultiply_returnD
    
    push hl
    push de
    push bc
    
    ; Now we know neither operands are 255, increment both of them.
    inc d
    inc e
    
    ; hl = operand0, zero
    ; de = zero, operand1
    ld h, d
    ld d, 0
    ld l, d
    
    ; Loop 8 times
    ld b, 8
    util_fractionalMultiply_loop:
        ; Shift hl left by one, add de if there was a carry.
        add hl, hl
        jr nc, util_fractionalMultiply_skip
            add hl, de
        util_fractionalMultiply_skip:
    djnz util_fractionalMultiply_loop
    
    ; hl is now 16 bit result of multiplication, so decrement top byte and put in a.
    dec h
    ld a, h

    pop bc
    pop de
    pop hl
ret
util_fractionalMultiply_returnD:
    ld a, d
ret
util_fractionalMultiply_returnE:
    ld a, e
ret
