make:

	#-------- ASSEMBLING --------#
	z80asm src/main.zil -o compile/main.bin
	cat compile/main.bin | wc -c

	#----- MAKING TAPE FILE -----#
	/opt/bin2tap compile/main.bin compile/main.tap 23953

	#---------- RUNNING ---------#
	fuse-gtk --tape compile/main.tap --graphics-filter 3x --machine 16

noise:
	/opt/zxtap-to-wav -i compile/main.tap -o compile/main.wav -f 11025
	mpv compile/main.wav
